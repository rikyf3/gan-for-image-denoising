import tensorflow as tf
import cv2 as cv
import os
import wandb
import numpy as np

AUTOTUNE = tf.data.experimental.AUTOTUNE
wandb.init(project='cycleganfordenoising', sync_tensorboard=True)

def convolution_layer(inputs, filter):
    conv = tf.keras.layers.Conv2D(filter, (7,7), padding='same', strides=1, dilation_rate=2)(inputs)
    conv = tf.keras.layers.LeakyReLU()(conv)
    conv = tf.keras.layers.BatchNormalization()(conv)
    conv = tf.keras.layers.Conv2D(filter, (7,7), padding='same', strides=1)(conv)
    conv = tf.keras.layers.LeakyReLU()(conv)
    conv = tf.keras.layers.BatchNormalization()(conv)
    pool = tf.keras.layers.Conv2D(filter, (2,2), padding='same', strides=2)(conv)

    return conv, pool

def deconvolution_layer(inputs, conv_concat, filter):
    conv = tf.keras.layers.Conv2DTranspose(filter, (4,4), padding='same', strides=2)(inputs)
    conv = tf.keras.layers.Conv2D(filter, (7,7), padding='same', strides=1, dilation_rate=2)(tf.keras.layers.concatenate([conv_concat, conv]))
    conv = tf.keras.layers.LeakyReLU()(conv)
    conv = tf.keras.layers.Conv2D(filter, (7,7), padding='same', strides=1)(conv)
    conv = tf.keras.layers.LeakyReLU()(conv)
    conv = tf.keras.layers.BatchNormalization()(conv)

    return conv

def build_model_generator(IMG_WIDTH, IMG_HEIGHT, CHANNELS):
    inputs = tf.keras.layers.Input(shape=(IMG_WIDTH, IMG_HEIGHT, CHANNELS))

    down1, pool1 = convolution_layer(inputs, 64)
    down2, pool2 = convolution_layer(pool1, 128)
    down3, pool3 = convolution_layer(pool2, 256)
    down4, pool4 = convolution_layer(pool3, 512)

    down5, _ = convolution_layer(pool4, 1024)

    up4 = deconvolution_layer(down5, down4, 512)
    up3 = deconvolution_layer(up4, down3, 256)
    up2 = deconvolution_layer(up3, down2, 128)
    up1 = deconvolution_layer(up2, down1, 64)

    outputs = tf.keras.layers.Conv2D(CHANNELS, (1,1), padding='same', strides=1, activation=tf.nn.sigmoid)(up1)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    return model

def build_model_discriminator(IMG_WIDTH, IMG_HEIGHT, CHANNELS):
    inputs = tf.keras.layers.Input(shape=(IMG_WIDTH, IMG_HEIGHT, CHANNELS))

    _, pool1 = convolution_layer(inputs, 128)
    _, pool2 = convolution_layer(pool1, 64)
    _, pool3 = convolution_layer(pool2, 32)
    conv = tf.keras.layers.Conv2D(16, (3,3), padding='same', strides=1, activation=tf.nn.relu)(pool3)
    flat = tf.keras.layers.Flatten()(conv)
    dense = tf.keras.layers.Dense(1, activation=tf.nn.sigmoid)(flat)

    model = tf.keras.Model(inputs=inputs, outputs=dense)
    return model

def normalize(image):
    image = tf.cast(image, dtype=tf.float32)
    image = image / 255.0
    return image

def add_noise(image):
    noise = tf.random.normal(tf.shape(image), stddev=15, mean=0, dtype=tf.float32)
    return image + noise

def discriminator_loss(real, generated, loss_obj):
    real_loss = loss_obj(tf.ones_like(real), real)
    generated_loss = loss_obj(tf.zeros_like(generated), generated)
    total = real_loss + generated_loss
    return total * 0.5

def generator_loss(generated, loss_obj):
    return loss_obj(tf.ones_like(generated), generated)

def cycle_loss(real, cycled, LAMDA):
    return LAMDA * tf.reduce_mean(tf.abs(real - cycled))

def identity_loss(real_x, same_x, LAMDA):
    return tf.reduce_mean(tf.abs(real_x - same_x))*0.5*LAMDA

generator_XY = build_model_generator(128, 128, 3)
generator_YX = build_model_generator(128, 128, 3)
discriminator_X = build_model_discriminator(128, 128, 3)
discriminator_Y = build_model_discriminator(128, 128, 3)

if generator_XY is None:
    print('ciao')

generator_XY_opt = tf.keras.optimizers.Adam(lr=2e-4, beta_1=0.5)
generator_YX_opt = tf.keras.optimizers.Adam(lr=2e-4, beta_1=0.5)
discriminator_X_opt = tf.keras.optimizers.Adam(lr=2e-4, beta_1=0.5)
discriminator_Y_opt = tf.keras.optimizers.Adam(lr=2e-4, beta_1=0.5)

def train_step(real_x, real_y):
    LAMDA = 10
    loss_obj = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    with tf.GradientTape(persistent=True) as tape:
        # X -> Y -> X
        fake_y = generator_XY(real_x, training=True)
        cycle_x = generator_YX(fake_y, training=True)

        # Y -> X -> Y
        fake_x = generator_YX(real_y, training=True)
        cycle_y = generator_XY(fake_x, training=True)

        #
        same_x = generator_YX(real_x, training=True)
        same_y = generator_XY(real_y, training=True)

        #
        d_real_x = discriminator_X(real_x, training=True)
        d_fake_x = discriminator_X(fake_x, training=True)

        d_real_y = discriminator_Y(real_y, training=True)
        d_fake_y = discriminator_Y(fake_y, training=True)

        #
        gen_XY_loss = generator_loss(d_fake_y, loss_obj)
        gen_YX_loss = generator_loss(d_fake_x, loss_obj)

        tot_cycle_loss = cycle_loss(real_x, cycle_x, LAMDA) + cycle_loss(real_y, cycle_y, LAMDA)

        tot_generator_XY_loss = gen_XY_loss + tot_cycle_loss + identity_loss(real_x, same_x, LAMDA)
        tot_generator_YX_loss  = gen_YX_loss + tot_cycle_loss + identity_loss(real_y, same_y, LAMDA)

        #
        d_x_loss = discriminator_loss(d_real_x, d_fake_x, loss_obj)
        d_y_loss = discriminator_loss(d_real_y, d_fake_y, loss_obj)

        #calculate gradients
        generator_XY_grad = tape.gradient(tot_generator_XY_loss, generator_XY.trainable_variables)
        generator_YX_grad = tape.gradient(tot_generator_YX_loss, generator_YX.trainable_variables)

        discriminator_X_grad = tape.gradient(d_x_loss, discriminator_X.trainable_variables)
        discriminator_Y_grad = tape.gradient(d_y_loss, discriminator_Y.trainable_variables)

        #apply gradients
        generator_XY_opt.apply_gradients(zip(generator_XY_grad, generator_XY.trainable_variables))
        generator_YX_opt.apply_gradients(zip(generator_YX_grad, generator_YX.trainable_variables))
        discriminator_X_opt.apply_gradients(zip(discriminator_X_grad, discriminator_X.trainable_variables))
        discriminator_Y_opt.apply_gradients(zip(discriminator_Y_grad, discriminator_Y.trainable_variables))
        parameters = {
            'generator_XY_loss':float(tot_generator_XY_loss),
            'generator_YX_loss':float(tot_generator_YX_loss),
            'discriminator_X_loss':float(d_x_loss),
            'discriminator_Y_loss':float(d_y_loss)
        }
        wandb.log(parameters)

        print('Generator: {}\t\tDiscriminator: {}'.format(float(tot_generator_XY_loss), float(d_x_loss)))

class Create_file():
  def __init__(self, file_path, name='document.txt'):
    self.files = []
    self.document = open(name, 'w')

  def add_path(self, path):
    folder = os.listdir(path)

    for f in folder:
      if '.jpg' in f.lower():
        self.files.append(path+'/'+f)

  def generate(self):
    for f in self.files:
      self.document.write('{}\n'.format(f))
    self.document.close()

  def get_number_of_files(self):
    return len(self.files)

  def get(self):
    return self.files

def main():
    #
    dataset = Create_file('./')
    dataset.add_path('./Training_set')

    epochs = 5
    LAMDA = 10

    for e in range(1, epochs+1):
        for image_path in dataset.get():
            image = cv.imread(image_path)
            if image is None:
                print('ciao')
                continue
            image = cv.resize(image, (128, 128))
            image = normalize(image)
            image_noisy = add_noise(image)

            if image is None or image_noisy is None:
                continue

            train_step(tf.reshape(image, [1, 128, 128, 3]), tf.reshape(image_noisy, [1, 128, 128, 3]))
if __name__ == '__main__':
    main()
    generator_XY.save('generator_XY2.h5')
    generator_YX.save('generator_YX2.h5')
    discriminator_X.save('discriminator_X2.h5')
    discriminator_Y.save('discriminator_Y2.h5')
    wandb.save('generator_XY2.h5')
    wandb.save('generator_YX2.h5')
    wandb.save('discriminator_X2.h5')
    wandb.save('discriminator_Y2.h5')
